---
layout: post
title: "How do I use UFW? Easy!"
date: 2018-09-24T04:52:43-07:00
author: RetroMe
summary: >
  This is a primer on UFW. I discuss how and why to activate a firewall on your
  Linux box.
categories: software
thumbnail: fa-microchip
tags:
 - software
 - ufw
 - the-best
 - firewall
 - how to
---

# UFW Makes Firewalling Easy

Many distributions of Linux support the [Uncomplicated Firewall (UFW)][ufwwiki]. 
This program is for managing your firewall easily and simply from the command
line.  There are graphical user interfaces for UFW such as [Gufw][gufwlnk] but
I will not discuss them here.

## Installing UFW

```
$ sudo pacman -S ufw
```

## Setup

```
$ sudo ufw allow ssh
$ sudo ufw allow http
$ sudo ufw allow https
```

Alternatively you can also use ports in substitution of service names.

```
$ sudo ufw allow 22
$ sudo ufw allow 80
$ sudo ufw allow 443
```

## Activation

We finish by defaulting the firewall to block any incoming connection that we
do not explicitly allow.

```
$ sudo ufw default deny incoming
$ sudo ufw enable
```

## Integrating Fail2Ban with UFW

```
$ sudo cp /etc/fail2ban/jail.conf /etc/fail2ban/jail.local
$ sudo vim /etc/fail2ban/jail.local
```

Locate the banaction and change the value to ufw.

```
banaction = ufw
```

This enables integration between UFW and Fail2Ban.

## Why do I need a firewall?

While NAT and the built in processes available on most home routers may be
sufficient for some, I recommend activating your firewall and managing it for
two reasons. The first reason is that you want to have a greater understanding
and control over your own home. Do you have locks on your house? Same concept.
The second reason is because your computer may not be the main target of an
attack but could be a lateral step in an attackers vectoring. If someone like a
family member opens your network to attack and your system is not defended you
could become part of the breach or fall victim as well because you were unable
to defend against an internal attack. Defend yourself.

## Further Reading

[UFW on the Arch Wiki][archufw].

[gufwlnk]: http://gufw.org/ 'The Graphical UFW Site'
[ufwwiki]: https://en.wikipedia.org/wiki/Uncomplicated_Firewall 'The UFW Wiki'
[archufw]: https://wiki.archlinux.org/index.php/Uncomplicated_Firewall 'The arch wiki for UFW'
