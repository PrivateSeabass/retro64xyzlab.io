---
layout: post
title: "Book Study - Industrial Society and its Future"
date: 2018-10-22T15:52:43-07:00
author: RetroMe
summary: >
  Industrial Society and its Future is also known as The Unabomber Manifesto.
  This 35,000 word manifesto was given to several news agencies by Ted
  Kaczynski for publishing in exchange for refraining from sending a bomb and
  killing others. The FBI and the Attorney General at the time urged the
  publishing of the manifesto to protect the public.
categories: books
thumbnail: fa-book
tags:
 - ted kaczynski
 - book review
 - freedom club
 - manifesto
---

Please note that this is a book review for a manifesto written by a domestic
terrorist who conducted several serious attacks over a period of many years.
This review contains political commentary and possible violent rhetoric.

### A reading of The Unabomber Manifesto by The Revolutionary Conservative

<div class="video-container">
<iframe width="560" height="315" src="https://www.youtube.com/embed/gl9DgAM2zSk"
	frameborder="0" allowfullscreen></iframe>{: itemprop="associatedMedia" itemscope itemtype="http://schema.org/VideoObject"}
</div>

## Introduction

This is a book review, report, or interpretation. It contains opinion, fact,
and conjecture. I recommend [reading the text][manifesto] and digesting it yourself.

Ted Kaczynski, author of Industrial Society and its Future, believed that
technology was a major contributing factor to the downfall of society. His
opening statement is 'The Industrial Revolution and its consequences have been
a disaster for the human race.' and it sums up the entirety of his manifesto.
His acknowledgment and disagreement with many of the benefits that technology
have brought to humanity is the undertone for his disgust with the direction
that the world is headed.

Kaczynski believes that increased life span and reduced participation in life
have led to a unfulfilling existence that is subjected to indignities and
widespread psychological as well as physical suffering. He felt that technology
was exploitative of the natural order and caused severe damage to our planet.
Kaczynski was deeply concerned that an industrial and technologically advanced
society could only breed a world that deprives man of dignity and autonomy.

Kaczynski advocated for the destruction of the current system regardless of the
cost of human life because he felt that the positive benefits of autonomy and
freedom were far more valuable than the current economic and technological
basis for the present society. He stated very clearly that his movement was not
to be one of politics but of a return to simpler times with greater self
determination.

## leftism

An explanation of the leftist movement was provided by the author and he
believed it important to explain the ideology of the leftist in order to
distance his movement from what he felt was a culture of self hating
"politically correct" types who focused deeply on topics that involved two main
concepts. The psychological tendencies that underlie modern leftism are
proscribed by Kaczynski as "feelings of inferiority" and "over-socialization".

The author felt it was very important to stress that he interpreted leftism as
a culture that esteemed low self-esteem, feelings of powerlessness, depressive
tendencies, defeatism, guilt, self-hatred, and other psychological traits that
were each decisive traits in how participants will determine the direction of
the movement itself. I quote Ted Kaczynski directly in order to best understand
his feelings about leftism.

    16. Words like “self-confidence”, “self-reliance”, “initiative”,
	   “enterprise”, “optimism”, etc., play little role in the liberal and
	   leftist vocabulary. The leftist is antiindividualistic,
	   pro-collectivist. He wants society to solve every one’s problems for
	   them, satisfy everyone’s needs for them, take care of them. He is not
	   the sort of person who has an inner sense of confidence in his ability
	   to solve his own problems and satisfy his own needs. The leftist is
	   antagohistic to the concept of competition because, deep inside, he
	   feels like a loser

While a focus on inferiority was one of the issues that the author had a
problem with, he also focused on 'socialization' and the manner in which
Leftist culture is deeply focused on the expectations of society. The Leftist
will often claim to be a rebel or some kind of outside thinker but is in fact a
ruse put up in order to assuage feelings of guilt brought about by a lacking of
moral explanation for feelings and actions with nonmoral origin. A Leftist
feels guilt for natural urges and shame for thoughts and actions that are
outside the norms of behavior.

    28. The leftist of the oversocialized type tries to get off his
	   psychological leash and assert his autonomy by rebelling.  But usually
	   he is not strong enough to rebel against the most basic values of
	   society. Generally speaking, the goals of today’s leftists are NOT in
	   conflict with the accepted morality. On the contrary, the left takes an
	   accepted moral principle, adopts it as its own, and then accuses
	   mainstream society of violating that principle. Examples: racial
	   equality, equality of the sexes, helping poor people, peace as opposed
	   to war, nonviolence generally, freedom of expression, kindness to
	   animals.

The Leftist will claim to be a rebel through the practice of non threatening
behavior. They will tattoo themselves, attain piercings, or otherwise make
changes to their physical appearance in order to demonstrate their rebellious
attitude. They will then focus wholly on basic civil rights, relatively simple
concepts, or generally accepted attitudes and then push those concepts in a way
that reflects themselves as the only true gate keepers for society. They choose
morally good concepts, adopt those concepts, and then make claims that no one
else understands their deep and grand understanding of those concepts.
Kaczynski was very critical of leftism in his writings because he was concerned
that his movement may be co-opted by the Left and distorted in a way that would
not accomplish his goals. I feel he was concerned that leftism would adopt his
beliefs as a 'counter culture' but would in fact push for increased
technological development because of their need for goal attainment within what
he identified as the power process.

## Power Process

Kaczynski posits that every human being is driven by a biological need for
something he coined as the power process. The power process consists of
autonomy, goal, effort, and attainment of goal. The three simplest concepts are
the goal process. Autonomy he believed was something less clear cut and more
personal for each person and may not be necessary for all people. Modern
readers may recognize this as an early identification of [NPC theory][npcmeme].
The author believed that not every one had an inner dialogue that guides them.

The seasoned and wealthy may have fun in the short term but many will fall to
decadence, depression, and become acutely demoralized. This often manifests in
the wealthy turning to narcotics or hedonistic behavior such as the abuse of
children or peoples less fortunate then them. We see this in Europeans
traveling to Asia to satisfy their sexual needs with grander and more harmful
displays of domination on men, women, and children who are willing or unwilling
participants that are solely focused on their own basic survival needs.

The author does make it clear that not every one with money or wealth becomes a
monster. He notes that the emperor of Japan became a distinguished marine
biologist in order to satisfy his power process instead. He had no need to
worry about his survival so he instead developed surrogate activities and goals
that fulfilled his need to autonomously identify a goal, pursue it through
exertion, and eventually attain his objective. All peoples will focus on their
goals but the measure of those goals is demarcated by need and necessity.

## Autonomy

People may or may not need autonomy. However, there is a common theme in that
autonomous people are less likely to be participants in leftism. The autonomous
person will often work well in small groups where joint effort that is easily
measured can be seen.

People who are not autonomous will prefer to function as a member of a large
group. Their drive for power is weak and they are better able to satisfy their
needs by ascribing the success of others to themselves. You do not need to be
good at anything or contribute a significant amount in a Communistic society
because society provides a level playing field where the power process and
autonomy are not encouraged.

Kaczynski believes that a disruption in autonomy and the power process will
lead to demoralization, low self-esteem, inferiority feelings, defeatism,
depression, anxiety, guilt, frustration, spouse or child abuse, insatiable
hedonism, abnormal sexual behavior, sleep disorders, and eating disorders. He
believes that almost all social problems stem from individuals being unable to
participate in the power process and from their lack of autonomy.

## Social Breakdown

While the author does focus greatly on leftism, he believes that conservatism
is full of fools. The leftism will happily deploy technology to make rapid
social change with disregard for traditional values while the conservative
population complains. It is the money and capital provided by the conservatives
that bank rolls the development of that technology.

Whining about the decay of traditional values but enthusiastically supporting
technological progress and economic growth demonstrates a severe lack of
cognizance on the part of the conservative or right wing. You cannot make rapid
changes to a society without rapid changes in technology. Technology makes it
possible to destroy today and to replace it with tomorrow.

The traditional family unit, or nuclear family, is important in preindustrial
society. If you go far enough back in the time line we find polygamy an
important matter of survival as more wives and children that a man was able to
attain, the more likely he was able to send his genes forward. Technology has
no use for the family because technology becomes the family.

We see this social break down today in the abuse of social media, the
proliferation of broken homes, and the boom in prison population that can be
ascribed to the in between point between the nuclear family and total state
care of children. Single parent homes are [more likely to breed crime][singleparent].
Technology will one day breed homes with no parents.

## Technology Cannot Be Reformed

Society has failed to stop war, environmental degradation, political
corruption, drug trafficking, and domestic abuse. Kaczynski argues that even
the simplest of societal problems have been proven intractable and therefore
any belief that reformation of technology is a possibility would be a
falsehood. No social arrangements of laws, customs, or ethical codes can
provide protection against technology and the encroachment against our
freedoms.

Technology provides the force and pressure necessary to exert control over
human behavior and is therefore opposed to freedom. Technology can be used to
force others to conform to societal view points or can be employed to execute a
strike against a person if they are found to be outside the norm. Technology
exists as an extension of the system and it MUST use every practical means of
control over human behavior because human beings are the only thing we have
control over that could threaten the existence of the system.

Social disruption will be curbed to protect the system but we should remember
that technology will not be deployed with the intention of a totalitarian
government as the end goal. Technology will instead need to impose sufficient
control over people in small and measured responses to rational problems. We
will see cures for alcoholism develop from gene therapy which will later be
deployed to curb the crime rate through removal of aggression centers in the
brain. Positive possibilities will be the carrot that leads humanity towards a
total loss of freedom.

## Human Suffering

The destruction of the system through the regression of technology by
revolution will cause a serious break down in the current status quo.
Revolution will be violent and humans will die as part of the process. Our
planet cannot sustain the numbers we have reached without advanced technology.

If we cannot feed ourselves without advanced technology, it is apparent that
the regression in technological level will lead to mass starvation and human
suffering that is beyond belief. This is justified by the author by the belief
that the lowering of the birth rate will stabilize the death rate and the
process of rolling back industrialization will move from chaotic violence to a
symbiotic relationship with nature.

The author argues that human dignity and freedom will always trump a long life
or the avoidance of physical pain. He argues that a life is more valuable when
allowed to end in a fight for survival or for a cause than to live a long but
empty and purposeless life.

## The Future

Kaczynski worried deeply that technology could survive and eventually we would
find ourselves in a situation where intelligence machines are able to
accomplish almost all things better than humans. If those machines may make
their own autonomous decisions we will be unable to postulate what future we
may behold. If those machines are controlled by man we will find a greater and
greater concentration of power in the hands of the "bourgeois" types who will
exert complete control over the common man.

Since it is impossible to reconcile technology with freedom we can expect that
a survival of the system will only lead to greater and more gross violations of
our rights in order to better serve the system. Therefore the author posits
that dumping the 'whole stinking system and taking the consequences' would be
much better than allowing it to fester.

The author also warns of groups who may try to take over the revolution that
will grow in force to one day overthrow the industrial system.

    217. In earlier revolutions, leftists of the most powerhungry type,
	    repeatedly, have first cooperated with nonleftist revolutionaries, as
	    well as with leftists of a more libertarian inclination, and later
	    have double-crossed them to seize power for themselves. Robespierre
	    did this in the French Revolution, the Bolsheviks did it in the
	    Russian Revolution, the communists did it in Spain in 1938 and Castro
	    and his followers did it in Cuba. Given the past history of leftism,
	    it would be utterly foolish for non-leftist revolutionaries today to
	    collaborate with leftists.

Kaczynski desires social revolution and the total collapse of the technological
system but greatly fears that his movement could be taken over by individuals
who do not really see eye to eye with him.

## Conclusion

![unabomber][unabomber]

Ted Kaczynski wrote his manifesto because he believed that he could cause a
social revolution that would lead to greater freedom and the over throw of what
he believed to be despotic rule through technological means. He harbored a deep
resentment of both the political Right as well as the Left and he felt that it
was important to employ them in a way in which they could be controlled or
eliminated as he saw fit.

The pursuit of freedom and equality is posited to be available only in the
survival of the fittest culture provided by nature. The author believed that
the best way to make all men equal was to put them in a position where only
through survival could they be tested and no one who was not equal would
survive to feel discriminated against anyways. I believe that Kaczynski would
trade technology and all of her benefits for a return to something more aligned
with the animal nature that humanity rejects.

I like technology. I make a living from technology. I work in law enforcement
and I like law enforcement as both a concept as well as a career. While I
understand that Kaczynski wished to defend his definition of freedom, I also
feel that we can live in a world that embraces some technology without giving
up all of our freedoms. I also strongly believe that there is a natural flow to
how public sentiment embraces or rejects freedom and autonomy. Technology may
become a downfall for humanity but I also believe that the only way for
humanity to survive is to master technology. We really get one shot at it and I
don't think humanity can live without pen and paper, farming, and medicine.

[npcmeme]: http://archive.is/nnLSU 'The NPC Wojak meme'
[singleparent]: http://archive.is/D77yQ 'Single Parent Homes Breed Crime'
[unabomber]: /../assets/images/inserts/10222018-unabomber/unabomber.jpg 'Kaczynski, Stephen J. Dubner/Getty Images'
[manifesto]: /../assets/pdf/kaczynski.pdf 'Industrial Society and Its Future'
