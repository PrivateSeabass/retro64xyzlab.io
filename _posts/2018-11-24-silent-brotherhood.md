---                                                                             
layout: post
title: "Book Study - The Silent Brotherhood"
date: 2018-11-23T15:52:43-07:00
author: RetroMe
summary: >
  The Silent Brotherhood is a true crime story about individuals involved in a
  violent anti-government militia movement within the United States. The group
  'The Order' was active within the United States from 1983 to 1984 and
  conducted murders and robberies.
categories: books 
thumbnail: fa-book
tags:
 - book review
 - terrorism
 - crime
 - law enforcement
---

### A foreword

As a member of the law enforcement community, it is imperative that I read
voraciously. While motivation and capability can fluctuate wildly between
criminals and their organizations, their methods are often shared. Terrorism,
murder, bombings, weaponized sexual crimes, and robberies are often the glue
that binds these groups together into commonality. The book The Silent
Brotherhood is a look into the neo-Nazi movement of the 1980's, their tactics,
and the methodology by which they attempted to attain their goals.

## The Silent Brotherhood

**The Chilling Inside Story Of America's Violent Anti-Government Militia Movement**

Kevin Flynn and Gary Gerhardt wrote 'comfortably secure Americans are used to
thinking of terrorism as something that carries a foreign dateline...' in the
opening preface of their book. In the not so distant past of American
history our nation was embroiled in extreme violence as several groups jockeyed
for position in what they believed was a coming civil war based on political
ideology, skin color, and financial disparity. Groups such as The Order,
Weather Underground, Ku Klux Klan, and Black Panthers conducted operations
throughout the United States that included assassination, robbery, theft,
kidnapping, and terrorist acts.

This book covers the struggle between white nationalist or white identity groups
as they prepared for what they thought was an inevitable war between what they
called the 'Zionist Occupational Government' and ordinary citizens. The focus
of this novel was on the Silent Brotherhood, a private guerrilla army with the
goal of conducting terror and sabotage campaigns within the United States in
order to hasten the start of civil war II. This group was famous for obtaining
$4 million dollars in stolen money from bank and armored car robberies as well
as conducting the targeted assassination of Jewish talk show host Alan Berg in
Denver Colorado in 1984.

## Foundations

The Order was originally founded by Robert Jay Mathews in 1983 at his farm in
Metaline, Washington. The name 'The Order' came from a book called [The Turner
Diaries][turner] in which a fictionalized American revolution leads to the
violent overthrow of the United States Government. Mathews began his story as a
baptized member of the Mormon faith who felt that the church lacked the
extremism that he thirsted for. He moved on to form an anti-communist militia
before eventually evolving into a member of white identity and forming The
Order. 

## Attacks And Operations

The Order conducted operations such as counterfeiting money, requesting money
from Iran, robberies, and more. They also performed legal work such as timer
contracting and lumber-jacking. While they did perform legal work, they felt
that the appeal of counterfeiting was more conducive to their mission as it
also functioned as a direct attack on the legitimacy of the American
Government. Due to the groups believe that robbery was sinful, the group
attempted to only target pimps, drug dealers, and owners of pornography stores.
They felt this behavior worked in a double role of causing damage to street
criminals while also funding their ventures.

The group firebombed community centers, bombed pornography theaters, and
conducted attacks on Brinks armored cars. Eventually they netted nearly $4.0
million dollars that they gave away to groups like the National Alliance and
the White Patriot Party. Their intention was to fund the groups who they
believed would help usher in an advancement of white interests and a hastening
of what they believed was an inevitable race war.

## Aftermath

Ten members of The Order were convicted under the Racketeer Influenced and
Corrupt Organizations Act (RICO) after Frazier Glenn Miller, Jr testified
against them in exchanged for reduced sentencing. No members of The Order were
ever charged in the murder of Alan Berg. The results of the trials of these men
ranged from 252 years in prison for one member and acquittal for 14 other men.

Robert Jay Mathews, the founder of The Order died in a home on Whidbey Island
near Freeland Washington after engaging federal law enforcement in a heated gun
battle. Mathews fired over 1000 of ammunition before agents fired M-79
Starburst flares through the window of the home, igniting it on fire. The home
burned to the ground and agents were later able to find the charred remains of
Mathews with a pistol still clutched in his hands. Autopsy concluded that
Mathews died from burns and smoke inhalation. No federal agents were injured in
the gun battle.

# Conclusion

America has a vicious history of radical militant groups who have operated
within her borders since inception. While today our media focuses on Islamic
terrorism and flirts with Narco-Terrorism, there was a time that the militia
movement was of great concern to the powers that be. Leftist groups like The
Weather Underground and neo-Nazi groups like The Order conducted violent
operations throughout the nation. While many of these groups have disappeared
into history, it is important that we never forget the motives, tactics, or
capabilities of these groups. We must not forget that these groups once planted
bombs around the nation, struck terror in almost every state, and continue to
spread their messages through numerous organizations throughout the US with
histories steeped in warfare that many no longer remember.

You can purchase the book on [Amazon][amazon].

[turner]: https://en.wikipedia.org/wiki/The_Turner_Diaries 'Wiki: The Turner Diaries'
[amazon]: https://www.amazon.com/Silent-Brotherhood-Chilling-Americas-Anti-Government/dp/0451167864/ref=sr_1_1?ie=UTF8&qid=1543119698&sr=8-1&keywords=silent+brotherhood+chilling 'The Silent Brotherhood Amazon'
